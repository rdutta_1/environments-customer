#!/bin/sh

####################################################################################################
# This script will be invoked and executed on Connector host by he-installer after installing Connector
# features and before invoking health check step.

#
# Make sure all the dependent files are present in this directory.
#
# The he-installer passes the following arguments in the same order to this script to use if needed
# $1 => HealthRulesConnectorServer Location
# $2 => Connector distribution location name
# $3 => Username being used to connect to HealthRulesConnectorServer
# $4 => Hostname

##################################################################################################

# Uncomment the following line to invoke providersearch script to copy and configure the buleprints and create indices in Elasticsearch
#./providersearch.sh $@
./createFolder.sh