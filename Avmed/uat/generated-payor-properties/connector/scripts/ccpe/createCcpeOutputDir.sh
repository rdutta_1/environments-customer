#!/bin/bash
set -e

mkdir -p /home/cprime/correspondence/ccpe_output/data/BILL/OI_Invoices
mkdir -p /home/cprime/correspondence/ccpe_output/data/eob/completed/All
mkdir -p /home/cprime/correspondence/ccpe_output/data/eop/completed/All
mkdir -p /home/cprime/correspondence/ccpe_output/data/eop/completed/Member
mkdir -p /home/cprime/correspondence/ccpe_output/data/eop/completed/Provider
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Overage_Dependent_Letter_Small_Large_Group
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/COB_Other_Insurance_Monthly
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/PCP_Assignment
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Terminate_Dependent_for_Non_Response
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Accident_Workers_Compensation_MVA_First_Notice
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Accident_Workers_Compensation_MVA_Second_Notice
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Other_Coverage_Insurance_Investigation_for_Dependents
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Grandchild_Letter
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Request_for_Medical_Records_and_Supporting_Documentation
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Termination_Notice_for_NonPayment_Small_Group
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Dependent_Verification
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/ Returned_Payments_for_Small_Groups
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/Returned_Payments_for_IFP_Accounts
mkdir -p /home/cprime/job/correspondence/ccpe_output/outbound/customletter/State_of_Florida_45_Day_Pended_Claims

