env_type:         'prod'
env_type_acronym: 'pr'    #TODO ensure acronym is TWO characters

env_sla:          'prod'                   #TODO set SLA appropriately
env_status:       'live'
env_size:         'large'                #other options: medium,large
env_size_db:      'large'

# Payor Health Insurance Company Name (HIC)
payor_hic: Paramount Health Care 

#Do not Install care admin
#careadmin_enabled: False

# CareManager HE Installer custom files that customers would edit. See CAREMGR-29751.
# Enable below default feature to omit those manual steps during Caremanager,CareManager-careadmin New installation.
cm_custom_software:
  #ipg_and_pch_datafile:  ipg_and_pch_sample_data.sql
  #CA_EXT_datafile:       CA_EXT_1.2.0.sql


# TODO: Settings below are for illustration purposes. Versions need to be updated for this specific environment.
customer_software:
  payor:                 19.7.0.4-201909161650-eef21be 
  connector:             19.7.0.4-201909241142-5a6b38c
  connector_classic:     19.7-201907311837-4f45ad8
  lb:                    installed
  db:                    installed
  payor_dw:              installed
  #iway:                  installed
  #pik:                   19.1-201902040933-c9b862a
  #pik-lb:                installed
  #caremanager:           installed
  #caremanager_db:        installed
  #caremanager-careadmin: 18.2-RELEASE
  #printcomm:             19.6-20190612
  answers:               19.7
  #axiom-mssql:           installed
  #payor-dw-mssql:        installed
  #ces:                   installed
  #easygroup:             installed

