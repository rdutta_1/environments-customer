---
server_role:                "nexus"
cmdb_server_role:           "nexus"

sys_app_user:               "nexus"
sys_app_group:              "nexus"


yum_installs_group:
  - "epel-release"

firewalld_openports_group:
  - "8081/tcp"
  - "8443/tcp"
  - "9080-9083/tcp"

host_fs_layout:
  - vgname: vgNexus
    state: present
    filesystem:
      - {mntp: '/opt2', lvname: lvNexus, lvsize: 100%FREE, fstype: xfs}
    disk:
      - device: "{{ '/dev/xvdf' if provisioner == 'aws' else '/dev/sdb' }}"
        pvname: "{{ '/dev/xvdf1' if provisioner == 'aws' else '/dev/sdb1' }}"


host_fs_layout_vgdisks: "{%- for disk in item.disk -%} {{disk.pvname}} {%- if not loop.last -%},{%- endif -%}{% endfor %}"

java_owner: "{{ sys_app_user }}"
java_group: "{{ sys_app_group }}"

java_home_root: /opt/java
default_jdk: jdk8-current
java_home: "{{ java_home_root }}/{{ default_jdk }}"

java_distros:
  - java_home: "{{ java_home }}"
    java_distro: jdk-8u172-linux-x64.tar.gz
    java_ver_str: 1.8.0_172
    java_crypto_dir: UnlimitedJCEPolicyJDK8

nexus_version: '3.12.0-01'
nexus_installation_dir: '/opt'
nexus_data_dir: '/opt2/nexus-data'
nexus_backup_dir: '/opt2/nexus-backup'

nexus_min_memory: "9G"
nexus_max_memory: "27G"

## TODO: Dynamic hostname
public_hostname: "{{ nexus_public_hostname }}"
nexus_branding_header: "HealthEdge Nexus 3"
nexus_branding_footer: "Ansible last provisioned: {{ ansible_date_time.iso8601 }}"

nexus_timezone: 'America/New_York'  # java timezone
nexus_anonymous_access: True

nexus_ldap_realm: true

ldap_connections:
  - ldap_name: 'HealthEdge Auth LDAP' # used as a key to update the ldap config
    ldap_protocol: 'ldap' # ldap or ldaps
    ldap_hostname: 'pdcprodinfdc01.headquarters.healthedge.com'
    ldap_port: 389
    ldap_search_base: 'DC=headquarters,DC=healthedge,DC=com'
    ldap_auth: 'simple' # or simple
    ldap_auth_username: '{{ he_ldap_auth_username }}' # if auth = simple
    ldap_auth_password: '{{ he_ldap_auth_password }}' # if auth = simple
    ldap_user_base_dn: ''
    ldap_user_filter: '' # (optional)
    ldap_user_object_class: 'User'
    ldap_user_id_attribute: 'sAMAccountName'
    ldap_user_real_name_attribute: 'cn'
    ldap_user_email_attribute: 'mail'
    ldap_user_subtree: True
    ldap_map_groups_as_roles: false

nexus_email_server_enabled: True
nexus_email_server_host: "smtp1.healthedge.com"
nexus_email_from_address: "noreply@healthedge.com"

nexus_config_docker: True

nexus_docker_hosted_port: 9080
nexus_docker_group_port: 9082

nexus_repos_docker_hosted:
  - name: he-docker-hosted
    http_port: "{{ nexus_docker_hosted_port }}"
    v1_enabled: True
    write_policy: "allow"
    version_policy: "mixed"

nexus_repos_docker_group:
  - name: he-docker-group
    http_port: "{{ nexus_docker_group_port }}"
    v1_enabled: True
    member_repos:
      - he-docker-hosted
      - docker-proxy

nexus_privileges:
  - name: he-docker-all-priv
    description: 'All access to HealthEdge Docker repos'
    repository: 'he-docker-hosted'
    actions:
      - "*"

nexus_roles:
  - id: he-docker-admin-role
    name: he-docker-admin-role
    description: "HealthEdge Docker Admin"
    privileges:
      - he-docker-all-priv
    roles: []

nexus_local_users:
  - username: "he-docker-admin"
    first_name: HealthEdge
    last_name: Docker
    email: juttayaya@healthedge.com
    password: "{{ nexus_docker_password }}"
    roles:
      - he-docker-admin-role